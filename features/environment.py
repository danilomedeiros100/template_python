"""
If you want to run your test in headless mode, please uncomment line 18
"""
#
# import os
#
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
#
#
# from features.lib.pages.basepage import BasePage
# from features.lib.pages.loginpage import LoginPage
# from features.lib.pages.homepage import HomePage
#
# def before_all(context):
#
#     chrome_options = Options()
#     # chrome_options.add_argument("--headless")
#     chrome_options.add_argument("--window-size=1920x1080")
#     chrome_driver = os.getcwd() + '/features/lib/chromedriver.exe'
#
#     driver = webdriver.Chrome(
#         chrome_options=chrome_options, executable_path=chrome_driver)
#     driver.maximize_window()
#     driver.implicitly_wait(10)
#
#     context.browser = BasePage(driver)
#     context.login   = LoginPage(context)
#     context.home    = HomePage(context)
#
#
# def after_scenario(context, scenario):
#     context.browser.screenshot(str(scenario))
#
#
# def after_all(context):
#     print("===== That's all folks =====")
#     context.browser.close()
from appium.webdriver import webdriver


def before_scenario(context, scenario):
    udid = context.device
    url = context.url

    context.nomescenario = scenario.name
    context.scenariotags = scenario.tags

    desired_caps = {}
    desired_caps['platformName'] = "Android"
    desired_caps['udid'] = "ZY225FNTX8"
    desired_caps['deviceName'] = "moto_g_7_"
    desired_caps['appPackage'] = "com.globo.globotv"
    desired_caps['appActivity'] = "com.globo.globotv.splash.SplashActivity"
    desired_caps['noReset'] = True
    desired_caps['automationName'] = "uiautomator2"
    desired_caps['newCommandTimeout'] = 900
    desired_caps['no-reset'] = True
    driver = webdriver.Remote(url, desired_caps)
    context.driver = driver
    return context.driver