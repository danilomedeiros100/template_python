from pprint import pprint

from geopy.geocoders import Nominatim







from time import sleep

import reverse_geocoder as rg
from appium import webdriver

# udid = "0057160804"
# url = "https://br-sao.headspin.io:7012/v0/e0dfc76ad34c4b9882a4734ef479a891/wd/hub"


udid = "ZY225FNTX8"
url = "http://127.0.0.1:4723/wd/hub"

desired_caps = {}
desired_caps['platformName'] = "Android"
desired_caps['udid'] = udid
desired_caps['deviceName'] = udid
desired_caps['appPackage'] = "com.globo.globotv"
desired_caps['appActivity'] = "com.globo.globotv.home.HomeActivity"
desired_caps['noReset'] = True
desired_caps['automationName'] = "uiautomator2"
desired_caps['newCommandTimeout'] = 900
desired_caps['no-reset'] = True
driver = webdriver.Remote(url, desired_caps)
driver = driver

 #######################################



# Funçao de Coordenadas
location = driver.location
lat = location.get('latitude')
lon = location.get('longitude')


# Função de Localização
locator = Nominatim(user_agent='myGeocoder')
coordinates = lat, lon
location = locator.reverse(coordinates)
address= location.raw['address']


# ######## INFORMAÇÃO DO DEVICE #######
device_model = driver.desired_capabilities["deviceModel"]
platform_version = driver.desired_capabilities["platformVersion"]
platform_name = driver.desired_capabilities["platformName"]
device_name = driver.desired_capabilities["deviceName"]
# #############################

print("\n")
print("##### INFORMAÇÕES DO DEVICE ####")
print("MODELO DEVICE - ", device_model)
print("VERSÃO ANDROID - ", platform_version)
print("PLATAFORMA - ", platform_name)
print("UDID DEVICE - ", device_name)
 #########################################
print("\n")
print("######## INFORMAÇÃO DE LOCALIZAÇÃO E COORDENADAS #######")
print("Localização >>", address)
print("> Latitude - ", lat)
print("> Longitude - ", lon)

print("######## INFORMAÇÃO LOCALIZAÇÃO POR ITEM #######")
print("\n")

print("Cidade >>",address['city'])
print("Pais >>",address['country'])
print("Sigla pais >>",address['country_code'])
print("CEP >>",address['postcode'])
print("Rua/Avenida >>",address['road'])
print("Estado >>",address['state'])
print("Bairro >>",address['suburb'])
print("Região >>",address['region'])
print("<<<<<<<<<<<",address['county'])
print("<<<<<<<<<<<",address['road'])
print("<<<<<<<<<<<",address['state_district'])
